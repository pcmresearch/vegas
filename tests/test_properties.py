from vegas.core import SiteProperty
from vegas.core import LinkProperty
from vegas.core import System
from vegas.lattice import Lattice

import pytest


@pytest.fixture(scope='module')
def lattice():
    return Lattice((10, 10), pbc=(True, True))


@pytest.fixture(scope='module')
def system(lattice):
    return System(lattice=lattice)


@pytest.fixture(scope='module')
def sprop(system):
    bsp = SiteProperty(system, 1)
    assert bsp is not None
    return bsp


@pytest.fixture(scope='module')
def lprop(system):
    blp = LinkProperty(system, 1)
    assert blp is not None
    return blp


def test_base_site_property_can_be_instantiated(sprop):
    assert True


def test_base_site_property_keeps_dymensions(sprop):
    assert 1 == sprop.ndims


def test_base_site_property_keeps_data_dimensions(system, sprop):
    assert system.nsites == len(sprop.data)


def test_base_site_property_keeps_data_integrity(system, sprop):
    other_sprop = SiteProperty(system, 3)
    assert [0] * system.nsites == sprop.data
    assert [(0, 0, 0)] * system.nsites == other_sprop.data


def test_base_site_property_can_be_cyclic_initialized(system):
    bsp = SiteProperty(system, 3, [])
    assert [(0, 0, 0)] * system.nsites == bsp.data
    bsp = SiteProperty(system, 3, [1])
    assert [(1, 1, 1)] * system.nsites == bsp.data
    bsp = SiteProperty(system, 3, [1, 0, 0])
    assert [(1, 0, 0)] * system.nsites == bsp.data


def test_base_link_property_can_be_instantiated(lprop):
    assert True


def test_base_link_property_keeps_dymensions(lprop):
    assert 1 == lprop.ndims


def test_base_link_property_keeps_data_dimensions(system, lprop):
    assert system.nlinks == len(lprop.data)


def test_base_link_property_keeps_data_integrity(system, lprop):
    other_lprop = LinkProperty(system, 3)
    assert [0] * system.nlinks == lprop.data
    assert [(0, 0, 0)] * system.nlinks == other_lprop.data


def test_base_link_property_can_be_cyclic_initialized(system):
    bsp = LinkProperty(system, 3, [])
    assert [(0, 0, 0)] * system.nlinks == bsp.data
    bsp = LinkProperty(system, 3, [1])
    assert [(1, 1, 1)] * system.nlinks == bsp.data
    bsp = LinkProperty(system, 3, [1, 0, 0])
    assert [(1, 0, 0)] * system.nlinks == bsp.data
