import pytest

from vegas.lattice import Lattice


def test_pbc_value():
    lattice = Lattice((3, 3, 3))
    assert len(lattice.shape) == len(lattice.pbc)
    lattice = Lattice((3, 3, 3), pbc=False)
    assert len(lattice.shape) == len(lattice.pbc)


def test_pbc_consistent_with_shape():
    with pytest.raises(ValueError):
        Lattice((3, 3, 3), pbc=(False, False))


def test_pbc_are_false_is_none():
    lattice = Lattice((3, 3, 3))
    assert (False, ) * len(lattice.shape) == lattice.pbc


def test_index_indexes_correctly():
    lattice = Lattice((3, 3, 3))
    assert 0 == lattice.index(0, 0, 0)
    assert 1 == lattice.index(1, 0, 0)
    assert 13 == lattice.index(1, 1, 1)
    assert 26 == lattice.index(2, 2, 2)
    assert 19 == lattice.index(1, 0, 2)


def test_index_site_on_lattice():
    lattice = Lattice((4, 4, 4, 500))
    with pytest.raises(ValueError):
        lattice.index(0, 0, 0, 500)

    with pytest.raises(ValueError):
        lattice.index(0, 0, 0, -1)


def test_index_with_pbc():
    lattice = Lattice(shape=(5, 5, 5), pbc=(True, False, False))
    assert lattice.index(4, 0, 0) == lattice.index(-1, 0, 0)
    with pytest.raises(ValueError):
        lattice.index(0, -1, 0)


def test_site_consistence():
    lattice = Lattice(shape=(5, 5, 5))
    with pytest.raises(ValueError):
        lattice.index(0, 3, 0, 0, 0)
    with pytest.raises(ValueError):
        lattice.index(0, 3,)


def test_length_lattice():
    lattice = Lattice(shape=(10, 10, 10))
    assert 1000 == len(lattice)
    

def test_limits_neighbours():
    lattice = Lattice(shape=(10, 10, 10), pbc=(True, True, True))
    expected_limits = list(range(0, 6 * len(lattice) + 1, 6))
    limits, neighbours = lattice.adjacency()
    assert expected_limits == limits
    assert len(lattice) + 1 == len(limits)
    assert len(lattice) * 6 == len(neighbours)
    assert len(neighbours) == limits[-1]
    assert all([n < len(lattice) for n in neighbours])


def test_limits_neighbours_different_shape():
    lattice = Lattice(shape=(10, 5, 10), pbc=(True, True, True))
    expected_limits = list(range(0, 6 * len(lattice) + 1, 6))
    limits, neighbours = lattice.adjacency()
    assert expected_limits == limits
    assert len(lattice) + 1 == len(limits)
    assert len(lattice) * 6 == len(neighbours)
    assert len(neighbours) == limits[-1]
    assert all([n < len(lattice) for n in neighbours])


def test_limits_neighbours_different_dimension():
    lattice = Lattice(shape=(10, 5), pbc=(True, True))
    expected_limits = list(range(0, 4 * len(lattice) + 1, 4))
    limits, neighbours = lattice.adjacency()
    assert expected_limits == limits
    assert len(lattice) + 1 == len(limits)
    assert len(lattice) * 4 == len(neighbours)
    assert len(neighbours) == limits[-1]
    assert all([n < len(lattice) for n in neighbours])


def test_nbhs_of_site():
    lattice = Lattice(shape=(10, 10, 10), pbc=(True, True, True))
    nbhs = [1, 9, 10, 90, 100, 900]
    limits, neighbours = lattice.adjacency()
    assert nbhs == sorted(neighbours[:limits[1]])


def test_not_all_pbc():
    lattice = Lattice(shape=(10, 10, 10), pbc=(True, True, False))
    limits, neighbours = lattice.adjacency()
    nbhs = [1, 9, 10, 90, 100]
    assert not all(hi - low == 6 for low, hi in zip(limits[:-1], limits[1:]))
    assert nbhs == sorted(neighbours[:limits[1]])


def test_only_pbc_in_a_direction():
    lattice = Lattice(shape=(10, 10), pbc=(True, False))
    limits, neighbours = lattice.adjacency()
    nbhs = [1, 9, 10]
    assert not all(hi - low == 6 for low, hi in zip(limits[:-1], limits[1:]))
    assert nbhs == sorted(neighbours[:limits[1]])