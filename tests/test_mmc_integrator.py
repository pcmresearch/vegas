import sys
import pytest

from vegas.core import System
from vegas.core import State
from vegas.core import LinkProperty
from vegas.core import ExchangeHamiltonian
from vegas.core import MMCIntegrator
from vegas.lattice import Lattice


@pytest.fixture(scope='module')
def lattice():
    return Lattice((10, 10, 10), pbc=(True, True, True))


@pytest.fixture(scope='module')
def system(lattice):
    return System(lattice=lattice)


@pytest.fixture(scope='module')
def exchange(system):
    return LinkProperty(system, 1, [1, ])


@pytest.fixture(scope='module')
def hamiltonian(system, exchange):
    return ExchangeHamiltonian(system, exchange)


@pytest.fixture(scope='module')
def upstate(system):
    return State(system, [0, 0, 1])


@pytest.fixture(scope='module')
def meta_state(system):
    return State(system,
                 [0, 0, -1] + [0, 0, 1] * (system.nsites - 1))


@pytest.fixture(scope='function')
def integrator(hamiltonian):
    _i = MMCIntegrator(hamiltonian, seed=6969)
    assert _i is not None
    return _i


def norm(spin):
    x, y, z = spin
    return (x * x + y * y + z * z) ** 0.5


def test_mmc_integrator_is_instantiable(integrator):
    assert True


def test_mmc_integrator_gives_me_a_state_in_return(integrator, upstate):
    new_state = integrator.integrate(upstate, temperature=0.0)
    assert new_state is not upstate


def test_mmc_integrator_preserves_the_norm_of_spins(integrator, upstate):
    new_state = integrator.integrate(upstate, temperature=0.0)
    norms = [norm(spin) for spin in new_state.data]
    assert all(abs(norm - 1.0) < sys.float_info.epsilon for norm in norms)
    new_state = integrator.integrate(upstate, temperature=4.0)
    norms = [norm(spin) for spin in new_state.data]
    # Close enough
    assert all(abs(norm - 1.0) < 2 * sys.float_info.epsilon for norm in norms)


def test_mmc_integrator_preserves_upstate_at_zero(integrator, upstate):
    new_state = integrator.integrate(upstate, temperature=0.0)
    assert upstate.data == new_state.data


def test_mmc_integrator_shuffles_upstate_at_infinite(integrator, upstate):
    new_state = integrator.integrate(upstate, temperature=50.0)
    assert upstate.data != new_state.data


def test_mmc_integrator_minimizes_energy_even_at_zero(integrator,
                                                      upstate, meta_state):
    new_state = integrator.integrate(meta_state, temperature=0.0)
    assert meta_state.data[0] != new_state.data[0]
    assert upstate.data[1:] == new_state.data[1:]
