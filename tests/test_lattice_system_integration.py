from vegas.core import System
from vegas.lattice import Lattice


def test_system_can_be_instantiated():
    system = System()
    assert system is not None
    assert 0 == system.nsites
    assert 0 == system.nlinks


def test_system_can_be_instantiated_with_a_lattice():
    lattice = Lattice((10, 10), pbc=(True, True))
    system = System(lattice=lattice)
    _, neighbours = lattice.adjacency()
    assert len(lattice) == system.nsites
    assert len(neighbours) == system.nlinks
