import pytest

from vegas.core import System
from vegas.core import RawSystem


@pytest.fixture
def sys():
    return System()


def test_system_can_be_instantiated_from_python(sys):
    assert sys is not None


def test_base_system_exposes_its_number_of_sites(sys):
    assert sys.nsites == 0


def test_base_system_exposes_its_number_of_links(sys):
    assert sys.nlinks == 0


def test_instantiating_a_populated_system_with_lists():

    # a little system wit the folowing adjacency matrix
    #   0 1
    #   1 0

    sys = RawSystem(limits=[0, 1, 2], neighbours=[1, 0])
    assert sys is not None
    assert sys.nsites == 2
    assert sys.nlinks == 2
