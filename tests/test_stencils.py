import pytest

from vegas.core import SiteStencil
from vegas.core import LinkStencil
from vegas.core import System
from vegas.lattice import Lattice


@pytest.fixture(scope='module')
def lattice():
    return Lattice((10, 10), pbc=(True, True))


@pytest.fixture(scope='module')
def system(lattice):
    return System(lattice=lattice)


@pytest.fixture(scope='module')
def sstencil(system):
    sst = SiteStencil(system)
    assert sst is not None
    return sst


@pytest.fixture(scope='module')
def lstencil(system):
    lst = LinkStencil(system)
    assert lst is not None
    return lst


def test_site_stencil_can_be_instantiated(sstencil):
    assert True


def test_link_stencil_can_be_instantiated(lstencil):
    assert True


def test_site_stencil_keeps_the_number_of_sites(system, sstencil):
    assert system.nsites == len(sstencil.data)


def test_link_stencil_keeps_the_number_of_sites(system, lstencil):
    assert system.nlinks == len(lstencil.data)


def test_site_stencil_default_initializes(system, sstencil):
    assert [0] * system.nsites == sstencil.data


def test_link_stencil_default_initializes(system, lstencil):
    assert [0] * system.nlinks == lstencil.data


def test_site_stencil_cycle_initializes(system):
    sst = SiteStencil(system, [])
    assert [0] * system.nsites == sst.data
    sst = SiteStencil(system, [1])
    assert [1] * system.nsites == sst.data
    sst = SiteStencil(system, [1, 0])
    assert [1, 0] * (system.nsites // 2) == sst.data


def test_link_stencil_cycle_initializes(system):
    lst = LinkStencil(system, [])
    assert [0] * system.nlinks == lst.data
    lst = LinkStencil(system, [1])
    assert [1] * system.nlinks == lst.data
    lst = LinkStencil(system, [1, 0])
    assert [1, 0] * (system.nlinks // 2) == lst.data
