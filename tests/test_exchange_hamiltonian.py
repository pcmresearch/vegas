import pytest

from vegas.core import System
from vegas.core import State
from vegas.core import LinkProperty
from vegas.core import ExchangeHamiltonian
from vegas.lattice import Lattice


@pytest.fixture(scope='module')
def lattice():
    return Lattice((10, 10, 10), pbc=(True, True, True))


@pytest.fixture(scope='module')
def system(lattice):
    return System(lattice=lattice)


@pytest.fixture(scope='module')
def exchange(system):
    return LinkProperty(system, 1, [1, ])


@pytest.fixture(scope='module')
def hamiltonian(system, exchange):
    h = ExchangeHamiltonian(system, exchange)
    assert h is not None
    return h


@pytest.fixture(scope='module')
def upstate(system):
    return State(system, [0, 0, 1, ])


@pytest.fixture(scope='module')
def downstate(system):
    return State(system, [0, 0, -1, ])


def test_hamiltonian_can_be_instantiated(hamiltonian):
    assert True


def test_hamiltonian_yields_right_energy(system, hamiltonian, upstate):

    energies = [hamiltonian.energy(upstate, site)
                for site in range(system.nsites)]
    assert [-6] * system.nsites == energies

    exchange_negative = LinkProperty(system, 1, [-1, ])
    hamiltonian_negative = ExchangeHamiltonian(system, exchange_negative)
    energies = [hamiltonian_negative.energy(upstate, site)
                for site in range(system.nsites)]
    assert [6] * system.nsites == energies


def test_hamiltonian_yields_right_field(system, hamiltonian,
                                        upstate, downstate):
    for site in range(system.nsites):
        assert (0, 0, -6) == hamiltonian.field(upstate, site)
        assert (0, 0, 6) == hamiltonian.field(downstate, site)
