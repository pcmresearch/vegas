from vegas.core import State
from vegas.core import System
from vegas.lattice import Lattice

import pytest


@pytest.fixture(scope='module')
def lattice():
    return Lattice((10, 10), pbc=(True, True))


@pytest.fixture(scope='module')
def system(lattice):
    return System(lattice=lattice)


@pytest.fixture(scope='module')
def state(system):
    return State(system)


def test_base_state_can_be_instantiated_from_python():
    state = State(System())
    assert state is not None


def test_base_state_can_be_instantiated_from_python_with_list():
    state = State(System(), [])
    assert state is not None


def test_base_state_can_be_instantiated_from_python_with_system():
    state = State(System())
    assert state is not None


def test_base_state_preserves_nsites_and_nlinks(state):
    assert 100 == len(state.data)
    for i in state.data:
        assert 3 == len(i)


def test_base_state_does_not_freak_out_about_empty_lists(system):
    state = State(system, [])
    for i in state.data:
        assert (0, 0, 0) == i


def test_base_state_does_not_freak_out_about_one_element_lists(system):
    state = State(system, [3])
    for i in state.data:
        assert (3, 3, 3) == i


def test_base_state_does_not_freak_out_about_two_element_lists(system):
    state = State(system, [69, 112])
    for i, val in enumerate(state.data):
        # by Dr. Chuletas (Bowie Knife)
        if i % 2 == 0:
            assert (69, 112, 69) == val
        else:
            assert (112, 69, 112) == val


def test_base_state_does_not_freak_out_about_three_element_lists(system):
    state = State(system, [69, 112, 1])
    for i in state.data:
        assert (69, 112, 1) == i
