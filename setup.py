import sys
import pytest

from setuptools import setup
from setuptools import find_packages
from distutils.core import Command
from distutils.extension import Extension


class PyTest(Command):

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        errno = pytest.main([])
        sys.exit(errno)


include_dirs = [
    '/usr/include/boost',
    '/usr/local/include',
    'include'
]

libraries = [
    'boost_python-py34',
]

library_dirs = [
    '/usr/lib',
    '/usr/local/lib',
]

core_files = [
    'source/system.cc',
    'source/state.cc',
    'source/property.cc',
    'source/stencil.cc',
    'source/exchange_hamiltonian.cc',
    'source/mmc_integrator.cc',
    'source/core_wrapper.cc',
]

setup(
    name='vegas',
    ext_modules=[
        Extension('vegas._core',
                  core_files,
                  library_dirs=library_dirs,
                  libraries=libraries,
                  include_dirs=include_dirs,
                  extra_compile_args=['-std=c++11'],
                  depends=[],),
    ],
    tests_require=[
        'pytest>=2.7',
        'pytest-cov>=2.7'
    ],
    packages=find_packages(),
    install_requires=[
        'click',
    ],
    entry_points='''
    [console_scripts]
    vegas=vegas.scripts:vegas
    ''',
    cmdclass={
        'test': PyTest,
    }
)
