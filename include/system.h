#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "types.h"

class BaseSystem
{
    public:

        /*
         * Creates an unpopulated system
         */
        BaseSystem ();

        /*
         * Creates a system with its properties set
         */
        BaseSystem (const IndexArray&, const IndexArray&);

        /*
         * Destroys the system
         */
        virtual ~BaseSystem ();

        /*
         * Returns the number of sites in the system
         */
        IndexType nsites () const;

        /*
         * Returns the number of links in the system
         */
        IndexType nlinks () const;

        /*
         * Access to the limits
         */
        const IndexArray& limits () const;

        /*
         * Access to the neighbours
         */
        const IndexArray& neighbours () const;

    protected:
        IndexArray _limits;
        IndexArray _neighbours;
};

#endif

