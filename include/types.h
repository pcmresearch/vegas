#ifndef VEGAS_TYPES_H_
#define VEGAS_TYPES_H_

#include <array>
#include <vector>
#include <random>

typedef unsigned long int IndexType;
typedef std::vector<IndexType> IndexArray;
typedef std::array<IndexType, 3> IndexVector;

typedef double RealType;
typedef std::vector<RealType> RealArray;
typedef std::array<RealType, 3> RealVector;

typedef std::mt19937_64 RandomNumberGenerator;

typedef std::uniform_real_distribution<RealType> UniformRealDistribution;
typedef std::uniform_int_distribution<IndexType> UniformIndexDistribution;

#endif

