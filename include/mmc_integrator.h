#ifndef VEGAS_MMC_INTEGRATOR_H_
#define VEGAS_MMC_INTEGRATOR_H_

#include "types.h"
#include "system.h"
#include "property.h"
#include "state.h"
#include "exchange_hamiltonian.h"

#include <cmath>
#include <boost/math/constants/constants.hpp>

class BaseMMCIntegrator
{
public:

    BaseMMCIntegrator (IndexType);

    virtual ~BaseMMCIntegrator ();

    BaseState integrate (
            const BaseExchangeHamiltonian&,
            const BaseSystem&,
            const BaseLinkProperty&,
            const BaseState&,
            RealType);

private:

    RealVector _randomSpin ();

    RandomNumberGenerator _generator;

    UniformRealDistribution _uniform;

};

#endif
