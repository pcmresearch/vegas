#ifndef VEGAS_EXCHANGE_HAMILTONIAN_H_
#define VEGAS_EXCHANGE_HAMILTONIAN_H_

#include "types.h"
#include "system.h"
#include "state.h"
#include "property.h"

#include <boost/shared_ptr.hpp>


class BaseExchangeHamiltonian
{
public:

    /*
     * Builds an exchange hamiltonian for a given system and stores a reference
     * to the system, and stores a copy of the associated property
     */
    BaseExchangeHamiltonian ();

    /*
     * Destroys the hamiltonian
     */
    virtual ~BaseExchangeHamiltonian ();

    /*
     * Returns the local energy for a given state
     */
    RealType energy (
            const BaseSystem&, const BaseLinkProperty&,
            const BaseState&, IndexType) const;

    /*
     * Returns the field for a given site
     */
    RealVector field (
            const BaseSystem&, const BaseLinkProperty&,
            const BaseState&, IndexType) const;

};

#endif
