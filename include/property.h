#ifndef VEGAS_PROPERTY_H_
#define VEGAS_PROPERTY_H_

#include "types.h"
#include "system.h"


class BaseSiteProperty
{
public:

    /*
     * Build a site property for a given system spanning a given number of
     * items initialized in zero
     */
    BaseSiteProperty (const BaseSystem&, IndexType);

    /*
     * Build a site property for a given system spanning a given number of
     * items initialized accordingly using cycles
     */
    BaseSiteProperty (const BaseSystem&, IndexType, const RealArray&);

    /*
     * Destroys a site property
     */
    virtual ~BaseSiteProperty ();

    /*
     * Returns the number of dymensions of the input data
     */
    IndexType ndims () const;

    /*
     * Returns a const reference to the actual data
     */
    const RealArray& data () const;

protected:
    RealArray _data;
    IndexType _ndims;

};


class BaseLinkProperty
{
public:

    /*
     * Build a link property for a given system spanning a given number of
     * items initialized in zero
     */
    BaseLinkProperty (const BaseSystem&, IndexType);

    /*
     * Build a link property for a given system spanning a given number of
     * items initialized accordingly using cycles
     */
    BaseLinkProperty (const BaseSystem&, IndexType, const RealArray&);

    /*
     * Destroys a link property
     */
    virtual ~BaseLinkProperty ();

    /*
     * Returns the number of dymensions of the input data
     */
    IndexType ndims () const;

    /*
     * Returns a const reference to the actual data
     */
    const RealArray& data () const;


protected:
    RealArray _data;
    IndexType _ndims;

};

#endif
