#ifndef VEGAS_STATE_H_
#define VEGAS_STATE_H_

#include "types.h"
#include "system.h"

class BaseState
{
public:

    friend class BaseMMCIntegrator;

    /*
     * Builds a state for a given system
     */
    BaseState (const BaseSystem&);

    /*
     * Builds a state for a given system and initialize according
     * to the given initial array
     */
    BaseState (const BaseSystem&, const RealArray&);

    /*
     * Destroys the given state
     */
    virtual ~BaseState ();

    /*
     * Returns a const reference to the stored data
     */
    const RealArray& data () const;


protected:

    /*
     * Stores the state data in a flat array
     */
    RealArray _data;

};

#endif
