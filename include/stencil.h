#ifndef VEGAS_STENCIL_H_
#define VEGAS_STENCIL_H_

#include "types.h"
#include "system.h"


class BaseSiteStencil
{
public:

    /*
     * Builds a stencil for a given system
     */
    BaseSiteStencil (const BaseSystem&);

    /*
     * Builds a stencil for a given system and does cyclic initialization
     */
    BaseSiteStencil (const BaseSystem&, const IndexArray&);

    /*
     * Destroys the stencil
     */
    virtual ~BaseSiteStencil ();

    /*
     * Accessor for the stencil data
     */
    const IndexArray& data () const;

private:

    /*
     * Array containing the data
     */
    IndexArray _data;

};


class BaseLinkStencil
{
public:

    /*
     * Builds a stencil for a given system
     */
    BaseLinkStencil (const BaseSystem&);

    /*
     * Builds a stencil for a given system and does cyclic initialization
     */
    BaseLinkStencil (const BaseSystem&, const IndexArray&);

    /*
     * Destroys the stencil
     */
    virtual ~BaseLinkStencil ();

    /*
     * Accessor for the stencil data
     */
    const IndexArray& data () const;

private:

    /*
     * Array containing the data
     */
    IndexArray _data;

};

#endif
