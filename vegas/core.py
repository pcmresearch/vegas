from ._core import BaseSystem
from ._core import BaseState
from ._core import BaseSiteProperty
from ._core import BaseLinkProperty
from ._core import BaseSiteStencil
from ._core import BaseLinkStencil
from ._core import BaseExchangeHamiltonian
from ._core import BaseMMCIntegrator


class System(BaseSystem):

    def __init__(self, lattice=None):
        if lattice is not None:
            limits, neighbours = lattice.adjacency()
            super().__init__(limits, neighbours)
        else:
            super().__init__()


class RawSystem(BaseSystem):

    def __init__(self, limits=None, neighbours=None):
        super().__init__(list(limits or []),
                         list(neighbours or []))


class State(BaseState):
    pass


class SiteProperty(BaseSiteProperty):
    pass


class LinkProperty(BaseLinkProperty):
    pass


class SiteStencil(BaseSiteStencil):
    pass


class LinkStencil(BaseLinkStencil):
    pass


class ExchangeHamiltonian(BaseExchangeHamiltonian):

    def __init__(self, system, exchange):
        self.system = system
        self.exchange = exchange
        super().__init__()

    def energy(self, state, site):
        return super().energy(self.system, self.exchange, state, site)

    def field(self, state, site):
        return super().field(self.system, self.exchange, state, site)


class MMCIntegrator(BaseMMCIntegrator):

    def __init__(self, hamiltonian, seed=696969):
        self.hamiltonian = hamiltonian
        super().__init__(seed)

    def integrate(self, state, temperature):
        return super().integrate(
            self.hamiltonian,
            self.hamiltonian.system,
            self.hamiltonian.exchange,
            state,
            temperature)
