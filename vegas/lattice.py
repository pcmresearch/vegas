'''
Create some basic lattices for vegas
'''

import functools
import itertools
import operator


class Lattice(object):

    '''
    Creates a limits and neighbours list
    '''

    def __init__(self, shape, pbc=None):
        if pbc and len(shape) != len(pbc):
            raise ValueError('The lens of shape and pbc are different.')
        self.shape = shape
        self.pbc = pbc or (False, ) * len(shape)

    def __len__(self):
        return functools.reduce(operator.mul, self.shape, 1)

    def __site_pbc(self, site):
        return tuple((i % s if p else i)
                     for i, s, p in zip(site, self.shape, self.pbc))

    def __contains(self, site):
        return (all(i < j for i, j in zip(site, self.shape)) and
                all(i >= 0 for i in site))

    def __sites(self):
        return itertools.product(*map(range, self.shape))

    def __nbh_sites(self, site):
        cannonical = [1] + [0] * (len(self.shape) - 1)
        for delta in set(itertools.permutations(cannonical)):
            yield tuple(map(operator.add, site, delta))
            yield tuple(map(operator.sub, site, delta))

    def index(self, *site):
        '''
        Returns the index of a given site, if the lattice is periodic, it will
        ensure that the site is inbound, otherwise if the site is out of bounds
        it'll raise a value error
        '''

        if len(site) != len(self.shape):
            raise ValueError('The lens of site and shape are different ')

        site_pbc = tuple(self.__site_pbc(site))
        if not self.__contains(site_pbc):
            raise ValueError('The indices are out of lattice')

        weights = list(itertools.accumulate(self.shape, operator.mul))
        weights = [1] + weights[:-1]
        return sum(map(operator.mul, weights, site_pbc))

    def adjacency(self):
        '''
        Returns the adjacency list of the lattice in CSR format
        '''
        limits = [0]
        neighbours = []
        for site in self.__sites():
            for nbh in self.__nbh_sites(site):
                nbh_pbc = self.__site_pbc(nbh)
                if self.__contains(nbh_pbc):
                    neighbours.append(self.index(*nbh_pbc))
            limits.append(len(neighbours))
        return limits, neighbours
