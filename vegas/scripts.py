import click
import functools

from vegas.lattice import Lattice
from vegas.core import System
from vegas.core import State
from vegas.core import LinkProperty
from vegas.core import ExchangeHamiltonian
from vegas.core import MMCIntegrator


@click.group()
def vegas():
    pass


def spin_sum(a, b):
    return tuple((x + y for x, y in zip(a, b)))


def spin_norm(spin):
    x, y, z = spin
    return (x*x + y*y + z*z) ** 0.5


def magnetization(state):
    return functools.reduce(spin_sum, state, (0, 0, 0))


@vegas.command()
@click.argument('size', type=int)
@click.argument('temperature', type=float)
@click.option('--dimension', default=3,
              help='Number of dimensions of the system, defaults to 3 \
dimensions')
@click.option('--exchange', default=1,
              help='Value of the exchange between adjacent sites, defaults \
to 1 for ferromagnetic interaction')
@click.option('--periodic/--no-periodic', default=True,
              help='Periodic boundary conditions in all dimensions')
@click.option('--seed', default=6969,
              help='Default seed for the integrator')
@click.option('--steps', default=1000,
              help='Number of steps to run')
@click.option('--last-state/--no-last-state', default=False,
              help='Prints the last state instead of the magnetization \
per step')
@click.option('--random-initial/--no-random-initial', default=False,
              help='Use a random initial state instead of the pure up state')
def benchmark(size, temperature, dimension, exchange, periodic, seed, steps,
              last_state, random_initial):
    '''
    Runs a system for a fixed number of steps at a given temperature, the
    system is a square lattice in a given number of dimensions, you can chose
    to have either periodic or free boundary conditions and several other
    options.

    Parameters:

        SIZE            The linear size of the system

        TEMPERATURE     Temperature to run the system at


    '''

    click.echo('# Running the benchmark...')
    lattice = Lattice((size, ) * dimension, (periodic, ) * dimension)
    system = System(lattice=lattice)
    exchange_property = LinkProperty(system, 1, [exchange, ])
    hamiltonian = ExchangeHamiltonian(system, exchange_property)
    integrator = MMCIntegrator(hamiltonian, seed=seed)

    state = State(system, [0., 0., 1.])

    if random_initial:
        for _ in range(10):
            state = integrator.integrate(state, 100)

    for time in range(steps):
        state = integrator.integrate(state, temperature)
        mag = magnetization(state.data)
        info = [time, ] + list(mag) + [spin_norm(mag), ]
        if not last_state:
            click.echo(' '.join(map(str, info)))

    if last_state:
        for spin in state.data:
            click.echo(' '.join(map(str, spin)))

    click.echo('# Done!')


if __name__ == '__main__':
    vegas()
