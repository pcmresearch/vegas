# VEGAS

**warning:** This project relies in the boost python library installed in your
system.

In an Ubuntu system you should be able to install all the boost libraries using
the command:

```bash
sudo apt-get install libboost-all-dev
```

## Development setup

First you need to install the awesome `pew` package, which is available via pip

```bash
sudo pip3 install pew
```

Then you need to create a new development environment

```bash
git clone https://gitlab.com/pcmresearch/vegas.git
cd vegas
pew new vegas -d -p /usr/bin/python3 -r piprequirements.txt
```

The `-d` flag stands for _do not activate_, `-p` defines the python executable
to be used in this project, we recommend python3, finally the `-r` flags
installs python dependencies via pip.

## Testing

In order to test the project you should run:

```bash
pew in vegas python setup.py test
```

If you want to use the benchmark program to test things out,

```bash
pew in vegas vegas benchmark
```

## Contributing

Do all your changes in a feature branch,

```bash
git checkout -b feature-name
git push -u origin feature-name   # to set the branch in the server
```

Push your changes in progress to that feature branch, and then create
a new merge request when you are ready to go.

**Don't forget to add tests for your feature using pytest in the tests
directory**
