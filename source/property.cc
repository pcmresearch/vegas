#include "property.h"


BaseSiteProperty::BaseSiteProperty (const BaseSystem& system, IndexType ndims)
    : _ndims (ndims), _data (system.nsites () * ndims)
{
}


BaseSiteProperty::BaseSiteProperty (const BaseSystem& system, IndexType ndims, const RealArray& data)
    : _ndims (ndims), _data (system.nsites () * ndims)
{
    for (IndexType i = 0; i < _data.size (); ++i) {
        _data[i] = data[i % data.size ()];
    }
}


BaseSiteProperty::~BaseSiteProperty () { }


IndexType BaseSiteProperty::ndims () const
{
    return _ndims;
}


const RealArray& BaseSiteProperty::data () const
{
    return _data;
}



BaseLinkProperty::BaseLinkProperty (const BaseSystem& system, IndexType ndims)
    : _ndims (ndims), _data (system.nlinks () * ndims)
{
}


BaseLinkProperty::BaseLinkProperty (const BaseSystem& system, IndexType ndims, const RealArray& data)
    : _ndims (ndims), _data (system.nlinks () * ndims)
{
    for (IndexType i = 0; i < _data.size (); ++i) {
        _data[i] = data[i % data.size ()];
    }
}


BaseLinkProperty::~BaseLinkProperty () { }


IndexType BaseLinkProperty::ndims () const
{
    return _ndims;
}


const RealArray& BaseLinkProperty::data () const
{
    return _data;
}
