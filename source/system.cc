#include "system.h"


BaseSystem::BaseSystem ()
{
}


BaseSystem::BaseSystem (const IndexArray& limits, const IndexArray& neighbours)
    : _limits (limits), _neighbours (neighbours)
{
}


BaseSystem::~BaseSystem ()
{
}


IndexType BaseSystem::nsites () const
{
    if (_limits.size () == 0) return 0;
    return _limits.size () - 1;
}


IndexType BaseSystem::nlinks () const
{
    return _neighbours.size ();
}


const IndexArray& BaseSystem::limits () const
{
    return _limits;
}


const IndexArray& BaseSystem::neighbours () const
{
    return _neighbours;
}
