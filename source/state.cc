#include "state.h"


BaseState::BaseState (const BaseSystem& system)
    : _data (3 * system.nsites ())
{
}


BaseState::BaseState (const BaseSystem& system, const RealArray& data)
    : _data (3 * system.nsites ())
{
    for (IndexType i = 0; i < _data.size (); ++i) {
        IndexType j = i % data.size ();
        _data[i] = data[j];
    }
}


BaseState::~BaseState () {}


const RealArray& BaseState::data () const
{
    return _data;
}
