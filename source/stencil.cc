#include "stencil.h"


BaseSiteStencil::BaseSiteStencil (const BaseSystem& system)
    : _data (system.nsites ())
{
}


BaseSiteStencil::BaseSiteStencil (const BaseSystem& system, const IndexArray& data)
    : _data (system.nsites ())
{
    for (IndexType i = 0; i < _data.size (); i++)
        _data[i] = data[i % data.size ()];
}


BaseSiteStencil::~BaseSiteStencil () {}


const IndexArray& BaseSiteStencil::data () const
{
    return _data;
}



BaseLinkStencil::BaseLinkStencil (const BaseSystem& system)
    : _data (system.nlinks ())
{
}


BaseLinkStencil::BaseLinkStencil (const BaseSystem& system, const IndexArray& data)
    : _data (system.nlinks ())
{
    for (IndexType i = 0; i < _data.size (); i++)
        _data[i] = data[i % data.size ()];
}


BaseLinkStencil::~BaseLinkStencil () {}


const IndexArray& BaseLinkStencil::data () const
{
    return _data;
}
