#include "exchange_hamiltonian.h"


BaseExchangeHamiltonian::BaseExchangeHamiltonian ()
{
}


BaseExchangeHamiltonian::~BaseExchangeHamiltonian () {}


RealType BaseExchangeHamiltonian::energy (
        const BaseSystem& system,
        const BaseLinkProperty& exchange_prop,
        const BaseState& state,
        IndexType site) const
{
    const IndexArray& limits = system.limits ();
    const IndexArray& neighbours = system.neighbours ();
    const RealArray& spin = state.data ();
    const RealArray& exchange = exchange_prop.data ();

    RealType _energy = 0.0;

    for (IndexType j = limits[site]; j < limits[site + 1]; j++)
    {
        IndexType neighbour = neighbours[j];
        for (unsigned short alpha = 0; alpha < 3; alpha++)
            _energy -= exchange[j] * spin[site * 3 + alpha] * spin[neighbour * 3 + alpha];
    }

    return _energy;
}


RealVector BaseExchangeHamiltonian::field (
        const BaseSystem& system,
        const BaseLinkProperty& exchange_prop,
        const BaseState& state,
        IndexType site) const
{
    const IndexArray& limits = system.limits ();
    const IndexArray& neighbours = system.neighbours ();
    const RealArray& spin = state.data ();
    const RealArray& exchange = exchange_prop.data ();

    RealVector _field {0.0, 0.0, 0.0};

    for (IndexType j = limits[site]; j < limits[site + 1]; j++)
    {
        IndexType neighbour = neighbours[j];
        for (unsigned short alpha = 0; alpha < 3; alpha++)
            _field[alpha] -= exchange[j] * spin[neighbour * 3 + alpha];
    }

    return _field;
}
