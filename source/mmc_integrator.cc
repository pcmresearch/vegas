#include "mmc_integrator.h"


BaseMMCIntegrator::BaseMMCIntegrator (IndexType default_seed)
    : _generator (default_seed), _uniform (0.0, 1.0)
{
}


BaseMMCIntegrator::~BaseMMCIntegrator () {}


BaseState BaseMMCIntegrator::integrate (
            const BaseExchangeHamiltonian& hamiltonian,
            const BaseSystem& system,
            const BaseLinkProperty& exchange,
            const BaseState& old_state,
            RealType temperature)
{
    BaseState new_state = old_state;
    std::uniform_int_distribution<> rand_site (0, system.nsites () - 1);
    for (IndexType _ = 0; _ < system.nsites (); _++)
    {
        IndexType site = rand_site (_generator);
        RealType old_energy = hamiltonian.energy (system, exchange, new_state, site);
        RealVector new_spin = _randomSpin ();
        for (IndexType alpha = 0; alpha < 3; alpha++)
            new_state._data[site * 3 + alpha] = new_spin[alpha];
        RealType new_energy = hamiltonian.energy (system, exchange, new_state, site);
        RealType energy_delta = new_energy - old_energy;
        if (energy_delta < 0.0) continue;
        if (_uniform (_generator) < std::exp (- energy_delta / temperature)) continue;
        for (IndexType alpha = 0; alpha < 3; alpha++)
            new_state._data[site * 3 + alpha] = old_state._data[site * 3 + alpha];
    }
    return new_state;
}


RealVector BaseMMCIntegrator::_randomSpin ()
{
    using namespace boost::math::constants;
    RealType phi = 2.0 * pi<RealType> () * _uniform (_generator);
    RealType cos_theta = -1.0 + 2.0 * _uniform (_generator);
    RealType theta = std::acos (cos_theta);
    return RealVector {
            std::sin (theta) * std::cos (phi),
            std::sin (theta) * std::sin (phi),
            cos_theta
    };
}
