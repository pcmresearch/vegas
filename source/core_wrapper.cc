#include "types.h"

#include "system.h"
#include "state.h"
#include "property.h"
#include "stencil.h"

#include "exchange_hamiltonian.h"
#include "mmc_integrator.h"

#include <boost/python.hpp>


using namespace boost::python;


list real_array_to_list (const RealArray& array)
{
    list l = list ();
    for (auto && item : array) l.append (item);
    return l;
}


list index_array_to_list (const IndexArray& array)
{
    list l = list ();
    for (auto && item : array) l.append (item);
    return l;
}


list real_array_to_tuple_list (const RealArray& array, IndexType count)
{
    list l = list ();
    for (IndexType i = 0; i < array.size (); i += count)
    {
        list lt = list ();
        for (IndexType j = 0; j < count; j++)
            lt.append (array[i + j]);
        l.append (tuple (lt));
    }
    return l;
}


RealArray list_to_real_array (const list& data)
{
    IndexType size = len (data);
    RealArray _data (size);
    for (IndexType i = 0; i < size; ++i) {
        _data[i] = extract<RealType> (data[i]);
    }
    return _data;
}


IndexArray list_to_index_array (const list& data)
{
    IndexType size = len (data);
    IndexArray _data (size);
    for (IndexType i = 0; i < size; ++i) {
        _data[i] = extract<IndexType> (data[i]);
    }
    return _data;
}


static boost::shared_ptr<BaseState> makeState (
        const BaseSystem& system,
        const list& data)
{
    if (len (data) == 0) {
        return boost::shared_ptr<BaseState> (new BaseState (system));
    }
    auto _data = list_to_real_array (data);
    return boost::shared_ptr<BaseState> (new BaseState (system, _data));
}


list dataList (const BaseState& state)
{
    return real_array_to_tuple_list (state.data (), 3);
}


static boost::shared_ptr<BaseSystem> makeSystem (
        const list& limits,
        const list& neighbours)
{
    /*
     * TODO: This can be done with objects instead of lists
     */
    IndexArray _limits (len (limits));
    IndexArray _neighbours (len (neighbours));
    for (IndexType i = 0; i < _limits.size (); ++i)
        _limits[i] = extract<IndexType> (limits[i]);
    for (IndexType i = 0; i < _neighbours.size (); ++i)
        _neighbours[i] = extract<IndexType> (neighbours[i]);
    return boost::shared_ptr<BaseSystem> (new BaseSystem (_limits, _neighbours));
}


list sitePropDataList (const BaseSiteProperty& prop)
{
    if (prop.ndims () == 1) return real_array_to_list (prop.data ());
    return real_array_to_tuple_list (prop.data (), prop.ndims ());
}


static boost::shared_ptr<BaseSiteProperty> makeBaseSiteProperty (
        const BaseSystem& system,
        IndexType count,
        const list& data)
{
    if (len (data) == 0)
        return boost::shared_ptr<BaseSiteProperty> (new BaseSiteProperty (system, count));
    auto _data = list_to_real_array (data);
    return boost::shared_ptr<BaseSiteProperty> (new BaseSiteProperty (system, count, _data));
}


list linkPropDataList (const BaseLinkProperty& prop)
{
    if (prop.ndims () == 1) return real_array_to_list (prop.data ());
    return real_array_to_tuple_list (prop.data (), prop.ndims ());
}


static boost::shared_ptr<BaseLinkProperty> makeBaseLinkProperty (
        const BaseSystem& system,
        IndexType count,
        const list& data)
{
    if (len (data) == 0)
        return boost::shared_ptr<BaseLinkProperty> (new BaseLinkProperty (system, count));
    auto _data = list_to_real_array (data);
    return boost::shared_ptr<BaseLinkProperty> (new BaseLinkProperty (system, count, _data));
}


list siteStencilDataList (const BaseSiteStencil& stencil)
{
    return index_array_to_list (stencil.data ());
}


list linkStencilDataList (const BaseLinkStencil& stencil)
{
    return index_array_to_list (stencil.data ());
}


static boost::shared_ptr<BaseSiteStencil> makeBaseSiteStencil (
        const BaseSystem& system,
        const list& data)
{
    if (len (data) == 0)
        return boost::shared_ptr<BaseSiteStencil> (new BaseSiteStencil (system));
    auto _data = list_to_index_array (data);
    return boost::shared_ptr<BaseSiteStencil> (new BaseSiteStencil (system, _data));
}


static boost::shared_ptr<BaseLinkStencil> makeBaseLinkStencil (
        const BaseSystem& system,
        const list& data)
{
    if (len (data) == 0)
        return boost::shared_ptr<BaseLinkStencil> (new BaseLinkStencil (system));
    auto _data = list_to_index_array (data);
    return boost::shared_ptr<BaseLinkStencil> (new BaseLinkStencil (system, _data));
}


tuple tupleHamiltonianField (
        const BaseExchangeHamiltonian& hamiltonian,
        const BaseSystem& system,
        const BaseLinkProperty& exchange,
        const BaseState& state,
        IndexType site)
{
    RealVector field = hamiltonian.field (system, exchange, state, site);
    return make_tuple (field[0], field[1], field[2]);
}



BOOST_PYTHON_MODULE (_core)
{
    class_<BaseSystem, boost::shared_ptr<BaseSystem> > ("BaseSystem")
        .def ("__init__", make_constructor (makeSystem))
        .add_property ("nsites", &BaseSystem::nsites)
        .add_property ("nlinks", &BaseSystem::nlinks)
        ;

    class_<BaseState, boost::shared_ptr<BaseState> > ("BaseState", init<const BaseSystem&> ())
        .def ("__init__", make_constructor (makeState))
        .add_property ("data", &dataList)
        ;

    class_<BaseSiteProperty, boost::shared_ptr<BaseSiteProperty> > (
            "BaseSiteProperty",
            init<const BaseSystem&, IndexType> ())
        .def ("__init__", make_constructor (makeBaseSiteProperty))
        .add_property ("ndims", &BaseSiteProperty::ndims)
        .add_property ("data", &sitePropDataList)
        ;

    class_<BaseLinkProperty, boost::shared_ptr<BaseLinkProperty> > (
            "BaseLinkProperty",
            init<const BaseSystem&, IndexType> ())
        .def ("__init__", make_constructor (makeBaseLinkProperty))
        .add_property ("ndims", &BaseLinkProperty::ndims)
        .add_property ("data", &linkPropDataList)
        ;

    class_<BaseSiteStencil, boost::shared_ptr<BaseSiteStencil>> (
            "BaseSiteStencil",
            init<const BaseSystem&> ())
        .def ("__init__", make_constructor (makeBaseSiteStencil))
        .add_property ("data", &siteStencilDataList)
        ;

    class_<BaseLinkStencil, boost::shared_ptr<BaseLinkStencil>> (
            "BaseLinkStencil",
            init<const BaseSystem&> ())
        .def ("__init__", make_constructor (makeBaseLinkStencil))
        .add_property ("data", &linkStencilDataList)
        ;

    class_<BaseExchangeHamiltonian> ("BaseExchangeHamiltonian", init<> ())
        .def ("energy", &BaseExchangeHamiltonian::energy)
        .def ("field", &tupleHamiltonianField)
        ;


    class_<BaseMMCIntegrator> ("BaseMMCIntegrator", init<IndexType> ())
        .def ("integrate", &BaseMMCIntegrator::integrate)
        ;
}
